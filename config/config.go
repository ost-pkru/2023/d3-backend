package config

import (
	"errors"
	"gobasic/util/env"
	"log"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	Port          int
	DSN           string
	TokenSecret   string
	GatewayHost   string
	GatewayPrefix string
}

func Load() (Config, error) {
	// load from .env
	err := godotenv.Load()
	mode := env.GetEnv("APP_MODE", "development")
	if err != nil && mode != "production" {
		log.Fatal("Error loading .env file")
	}

	cfg := Config{
		Port:          env.GetEnvInt("APP_PORT", 8080),
		DSN:           os.Getenv("DB_DSN"),
		TokenSecret:   os.Getenv("TOKEN_SECRET"),
		GatewayHost:   os.Getenv("GATEWAY_HOST"),
		GatewayPrefix: os.Getenv("GATEWAY_PREFIX"),
	}

	// validate env
	if len(cfg.DSN) == 0 {
		return Config{}, errors.New("an error occurred while load config: DB_DSN is required")
	}

	if len(cfg.TokenSecret) == 0 {
		return Config{}, errors.New("an error occurred while load config: TOKEN_SECRET is required")
	}

	return cfg, nil
}
