package users

import (
	"gobasic/api/users/handler"
	db "gobasic/db/sqlc"
	"gobasic/util/token"

	"github.com/gofiber/fiber/v2"
)

func Init(r fiber.Router, q *db.Queries, tm *token.JWTMaker) {
	// create handler
	h := handler.NewUserHandler(q, tm)

	users := r.Group("/users")

	users.Post("", h.Signup)
	users.Post("/login", h.Signin)
}
