package dtos

import (
	"errors"
	"strings"
)

type SigninRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (r SigninRequest) Validate() error {
	errs := []string{}
	if len(r.Username) == 0 {
		errs = append(errs, "username is required")
	}
	if len(r.Password) == 0 {
		errs = append(errs, "password is required")
	}
	if len(errs) == 0 {
		return nil
	}
	return errors.New(strings.Join(errs, ", "))
}

type SigninResponse struct {
	Username string `json:"username"`
	Token    string `json:"token"`
}
