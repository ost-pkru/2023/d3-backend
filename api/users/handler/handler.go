package handler

import (
	"database/sql"
	"errors"
	"gobasic/api/users/dtos"
	db "gobasic/db/sqlc"
	"gobasic/util/pwd"
	"gobasic/util/token"
	"log"

	"github.com/gofiber/fiber/v2"
)

type UserHandler interface {
	Signup(c *fiber.Ctx) error
	Signin(c *fiber.Ctx) error
}

type userHandler struct {
	// ใช้ติดต่อฐานข้อมูล
	queries    *db.Queries
	tokenMaker *token.JWTMaker
}

func NewUserHandler(q *db.Queries, tm *token.JWTMaker) UserHandler {
	return &userHandler{
		queries:    q,
		tokenMaker: tm,
	}
}

// @Summary Create User
// @Description Create User
// @Tags User
// @Accept json
// @Produce json
// @Param user body dtos.SignupRequest true "User Data"
// @Failure 422
// @Failure 400
// @Failure 500
// @Success 201
// @Router /users [post]
func (h userHandler) Signup(c *fiber.Ctx) error {
	// Part 1: Handle Request JSON -> DTO
	form := dtos.SignupRequest{}
	if err := c.BodyParser(&form); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": err.Error(),
		})
	}
	// Validate username and password are required
	if err := form.Validate(); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": err.Error(),
		})
	}
	// Part 2: Business Logic
	// 2.1: check username already exists
	u, err := h.queries.GetUserByUsername(c.Context(), form.Username)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			log.Println(err)
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"error": "an error occurred while select user by username",
			})
		}
	}
	if u != (db.User{}) {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": "username already exists",
		})
	}
	// 2.2: hash password
	hash, err := pwd.HashPassword(form.Password)
	if err != nil {
		log.Println(err)
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": "an error occurred while hashing password",
		})
	}
	// Part 3: Save new user to database
	user := db.CreateUserParams{
		Username: form.Username,
		Password: hash,
	}
	_, err = h.queries.CreateUser(c.Context(), user)
	if err != nil {
		log.Println(err)
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": "an error occurred while create new user",
		})
	}

	// Part 4: Return response to client
	return c.SendStatus(fiber.StatusCreated)
}

// @Summary Login
// @Description Login
// @Tags User
// @Accept json
// @Produce json
// @Param user body dtos.SigninRequest true "User Data"
// @Failure 400
// @Failure 401
// @Failure 422
// @Failure 500
// @Success 200 {object} dtos.SigninResponse
// @Router /users/login [post]
func (h userHandler) Signin(c *fiber.Ctx) error {
	// Part 1: Handle Request JSON -> DTO
	form := dtos.SigninRequest{}
	if err := c.BodyParser(&form); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": err.Error(),
		})
	}
	// Validate username and password are required
	if err := form.Validate(); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": err.Error(),
		})
	}
	// Part 2: Business Logic
	// 2.1: check user is exists
	u, err := h.queries.GetUserByUsername(c.Context(), form.Username)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
				"error": "an user with given username is not found",
			})
		}
		log.Println(err)
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": "an error occurred while select user by username",
		})
	}

	// 2.2: check password
	match := pwd.CheckPasswordHash(form.Password, u.Password)
	if !match {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error": "username or passsword is incorrect",
		})
	}
	// Part 3: gennerate jwt token
	token, _, err := h.tokenMaker.CreateToken(u.ID.String(), u.Username)
	if err != nil {
		log.Println(err)
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": err.Error(),
		})
	}
	// Part 4: Return response to client
	resp := dtos.SigninResponse{
		Username: u.Username,
		Token:    token,
	}
	return c.JSON(resp)
}
