package handler

import (
	"database/sql"
	"errors"
	"gobasic/api/dashboards/dtos"
	"gobasic/middlewares"
	"gobasic/util/token"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

// @Summary Get User Balance
// @Description Get User Balance
// @Tags Dashboard
// @Produce json
// @Param Authorization header string true "Bearer"
// @Failure 401
// @Failure 500
// @Success 200 {object} dtos.UserBalanceResponse
// @Router /dashboards/balances [get]
func (h dashboardHandler) UserBalance(c *fiber.Ctx) error {
	// get user id
	payload := c.Locals(middlewares.AuthorizationPayloadKey).(*token.Payload)

	uid := uuid.MustParse(payload.Sub)

	row, err := h.queries.GetUserBalanceByUserId(c.Context(), uid)

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return c.JSON(dtos.UserBalanceResponse{})
		}
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": err.Error(),
		})
	}

	// serialize
	resp := dtos.NewUserBalanceResponse(row)

	return c.JSON(resp)
}
