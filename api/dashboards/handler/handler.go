package handler

import (
	db "gobasic/db/sqlc"

	"github.com/gofiber/fiber/v2"
)

type DashboardHandler interface {
	UserBalance(c *fiber.Ctx) error
}

type dashboardHandler struct {
	queries *db.Queries
}

func NewDashboardHandler(q *db.Queries) DashboardHandler {
	return &dashboardHandler{queries: q}
}
