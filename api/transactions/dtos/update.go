package dtos

type UpdateTransactionRequest struct {
	Title  string  `json:"title,omitempty"`
	Amount float64 `json:"amount,omitempty"`
}
