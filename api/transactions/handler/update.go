package handler

import (
	"database/sql"
	"errors"
	"gobasic/api/transactions/dtos"
	db "gobasic/db/sqlc"
	"gobasic/middlewares"
	"gobasic/util/token"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

// @Summary Update a transaction
// @Description Update a transaction by id
// @Produce json
// @Tags Transaction
// @Param Authorization header string true "Bearer"
// @Param id path string true "Transaction ID"
// @Failure 400
// @Failure 401
// @Failure 404
// @Failure 422
// @Failure 500
// @Success 200 {object} dtos.TransactionResponse
// @Router /transactions/{id} [patch]
func (h transactionHandler) Update(c *fiber.Ctx) error {
	id := c.Params("id")
	form := dtos.UpdateTransactionRequest{}
	if err := c.BodyParser(&form); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": err.Error(),
		})
	}

	// get user id
	payload := c.Locals(middlewares.AuthorizationPayloadKey).(*token.Payload)

	params := db.UpdateTransactionParams{
		ID:     uuid.MustParse(id),
		UserID: uuid.MustParse(payload.Sub),
	}
	if len(form.Title) > 0 {
		params.Title = sql.NullString{String: form.Title, Valid: true}
	}
	if form.Amount > 0 {
		params.Amount = sql.NullFloat64{Float64: form.Amount, Valid: true}
	}
	row, err := h.queries.UpdateTransaction(c.Context(), params)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
				"error": "transaction with given id is not found",
			})
		}
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": err.Error(),
		})
	}

	// serailize
	resp := dtos.NewTransactionResponse(row)
	return c.JSON(&resp)
}
