package handler

import (
	"database/sql"
	"errors"
	"gobasic/api/transactions/dtos"
	db "gobasic/db/sqlc"
	"gobasic/middlewares"
	"gobasic/util/token"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

// @Summary Get a transaction
// @Description Get a transaction by id
// @Produce json
// @Tags Transaction
// @Param Authorization header string true "Bearer"
// @Param id path string true "Transaction ID"
// @Failure 401
// @Failure 404
// @Failure 500
// @Success 200 {object} dtos.TransactionResponse
// @Router /transactions/{id} [get]
func (h transactionHandler) Get(c *fiber.Ctx) error {
	id := c.Params("id")
	// get user id
	payload := c.Locals(middlewares.AuthorizationPayloadKey).(*token.Payload)

	params := db.GetTransactionParams{
		ID:     uuid.MustParse(id),
		UserID: uuid.MustParse(payload.Sub),
	}
	row, err := h.queries.GetTransaction(c.Context(), params)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
				"error": "transaction with given id is not found",
			})
		}
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": err.Error(),
		})
	}

	// serailize
	resp := dtos.NewTransactionResponse(row)
	return c.JSON(&resp)
}
