package handler

import (
	"database/sql"
	"errors"
	db "gobasic/db/sqlc"
	"gobasic/middlewares"
	"gobasic/util/token"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

// @Summary Delete a transaction
// @Description Delete a transaction by id
// @Produce json
// @Tags Transaction
// @Param Authorization header string true "Bearer"
// @Param id path string true "Transaction ID"
// @Failure 400
// @Failure 401
// @Failure 404
// @Failure 422
// @Failure 500
// @Success 204
// @Router /transactions/{id} [delete]
func (h transactionHandler) Delete(c *fiber.Ctx) error {
	id := c.Params("id")
	// get user id
	payload := c.Locals(middlewares.AuthorizationPayloadKey).(*token.Payload)

	params := db.DeleteTransactionParams{
		ID:     uuid.MustParse(id),
		UserID: uuid.MustParse(payload.Sub),
	}
	err := h.queries.DeleteTransaction(c.Context(), params)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
				"error": "transaction with given id is not found",
			})
		}
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": err.Error(),
		})
	}

	return c.SendStatus(fiber.StatusNoContent)
}
