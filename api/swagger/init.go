package swagger

import (
	"fmt"
	"gobasic/config"
	"gobasic/docs"

	"github.com/gofiber/fiber/v2"
	fiberSwagger "github.com/swaggo/fiber-swagger"
)

func Init(r fiber.Router, cfg config.Config) {

	//Swagger Doc details
	host := cfg.GatewayHost
	basePath := cfg.GatewayPrefix

	if len(host) == 0 {
		host = fmt.Sprintf("localhost:%d", cfg.Port)
	}

	if len(basePath) == 0 {
		basePath = "/api/v1"
	}

	docs.SwaggerInfo.Title = "Expend API Document"
	docs.SwaggerInfo.Description = "List of APIs for Expend Service."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = host
	docs.SwaggerInfo.BasePath = basePath
	docs.SwaggerInfo.Schemes = []string{"https", "http"}

	r.Get("/swagger/*", fiberSwagger.WrapHandler)
}
