-- name: GetTransaction :one
SELECT * FROM public.transactions
WHERE id = $1 and user_id = $2;

-- name: ListTransactions :many
SELECT * FROM public.transactions
WHERE user_id = $1
AND (
  (sqlc.narg('start_date')::date is null)
  or
  (created_at >= sqlc.narg('start_date')::date)
)
AND (
  (sqlc.narg('end_date')::date is null)
  or
  (created_at <= sqlc.narg('end_date')::date)
)
ORDER BY created_at desc
LIMIT sqlc.arg(page_size)
OFFSET @page;

-- name: CountTransactions :one
SELECT count(*) FROM public.transactions
WHERE user_id = $1
AND (
  (sqlc.narg('start_date')::date is null)
  or
  (created_at >= sqlc.narg('start_date')::date)
)
AND (
  (sqlc.narg('end_date')::date is null)
  or
  (created_at <= sqlc.narg('end_date')::date)
);

-- name: CreateTransaction :exec
INSERT INTO public.transactions (type, title, amount, user_id) VALUES ($1, $2, @amount::float, $3);

-- name: CreateTransactionWithRow :one
INSERT INTO public.transactions (type, title, amount, user_id) VALUES ($1, $2, @amount::float, $3) RETURNING *;

-- name: UpdateTransaction :one
UPDATE public.transactions 
SET 
  title  = coalesce(sqlc.narg('title'), title),
  amount = coalesce(sqlc.narg('amount')::float, amount)
WHERE id = $1 and user_id = $2
RETURNING *;

-- name: DeleteTransaction :exec
DELETE FROM public.transactions WHERE id = $1 and user_id = $2;