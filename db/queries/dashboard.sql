-- name: GetUserBalanceByUserId :one
SELECT * FROM public.user_balance
WHERE user_id = $1
LIMIT 1;