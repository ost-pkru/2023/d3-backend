// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.16.0

package db

import (
	"database/sql/driver"
	"fmt"
	"time"

	"github.com/google/uuid"
)

type TransactionsType string

const (
	TransactionsTypeExpend TransactionsType = "expend"
	TransactionsTypeIncome TransactionsType = "income"
)

func (e *TransactionsType) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = TransactionsType(s)
	case string:
		*e = TransactionsType(s)
	default:
		return fmt.Errorf("unsupported scan type for TransactionsType: %T", src)
	}
	return nil
}

type NullTransactionsType struct {
	TransactionsType TransactionsType
	Valid            bool // Valid is true if TransactionsType is not NULL
}

// Scan implements the Scanner interface.
func (ns *NullTransactionsType) Scan(value interface{}) error {
	if value == nil {
		ns.TransactionsType, ns.Valid = "", false
		return nil
	}
	ns.Valid = true
	return ns.TransactionsType.Scan(value)
}

// Value implements the driver Valuer interface.
func (ns NullTransactionsType) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return ns.TransactionsType, nil
}

type Transaction struct {
	ID        uuid.UUID        `db:"id"`
	Type      TransactionsType `db:"type"`
	Title     string           `db:"title"`
	Amount    string           `db:"amount"`
	CreatedAt time.Time        `db:"created_at"`
	UserID    uuid.UUID        `db:"user_id"`
}

type User struct {
	ID        uuid.UUID `db:"id"`
	Username  string    `db:"username"`
	Password  string    `db:"password"`
	CreatedAt time.Time `db:"created_at"`
}

type UserBalance struct {
	UserID  uuid.UUID `db:"user_id"`
	Income  float64   `db:"income"`
	Expend  float64   `db:"expend"`
	Balance float64   `db:"balance"`
}
