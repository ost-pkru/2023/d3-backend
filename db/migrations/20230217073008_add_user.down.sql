ALTER TABLE public.transactions DROP CONSTRAINT transactions_fk;
ALTER TABLE public.transactions DROP COLUMN user_id;

DROP TABLE public.users;