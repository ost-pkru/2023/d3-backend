CREATE TABLE IF NOT EXISTS public.users (
  "id" uuid DEFAULT gen_random_uuid() PRIMARY KEY,
  "username" varchar(50) NOT NULL,
  "password" varchar(100) NOT NULL,
  "created_at" TIMESTAMPTZ NOT NULL DEFAULT current_timestamp
);

CREATE INDEX "users_username" ON public.users ("username");

ALTER TABLE public.transactions ADD user_id uuid NOT NULL;
ALTER TABLE public.transactions ADD CONSTRAINT transactions_fk FOREIGN KEY (user_id) REFERENCES public.users(id);