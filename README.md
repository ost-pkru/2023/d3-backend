# Course Expend API with Go

[Slide](https://drive.google.com/file/d/13xGAHuVzPp6BfwTd-8t3loHgcvQbmtB3/view?usp=share_link)

## Contents

[1](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/1) Main function

[2](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/2) Using makefile

[3](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/3) Module and Package

[4](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/4) Util get env

[5](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/5) HTTP Server with fiber

[6](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/6) Configurations

[7](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/7) New Server

[8](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/8) Basic routes

[9](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/9) Grouping routes

[10](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/10) Transactions module

[11](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/11) Use handler

[12](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/12) Read params

[13](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/13) Read query

[14](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/14) Using queryparser

[15](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/15) Read json

[16](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/16) Using DTO

[17](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/17) Handlering error response

[18](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/18) Global middlewares

[19](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/19) Using nigration init databae

[20](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/20) Open db connection

[21](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/21) Demo database/sql

[22](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/22) Using sqlc\*

[23](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/23) Migration add user

[24](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/24) Users module

[25](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/25) Signup dto

[26](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/26) Implementing signup handler

[27](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/27) Create sql for insert user

[28](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/28) Add dependency injection

[29](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/29) Add business logic

[30](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/30) Save new user to database

[31](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/31) Implement signin

[32](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/32) Authentication middleware

[33](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/33) Update transactions.sql\*

[34](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/34) Implementing transactions

[35](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/35) Serialize in DTO

[36](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/36) Add sql for dashboard

[37](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/37) Dashboard module

[38](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/38) Demo docs

[39](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/39) Create Dockerfile

[40](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/40) Using docker-compose

[41](https://gitlab.com/ost-pkru/2023/d3-backend/-/tree/41) Using gitlab-ci

## การสร้าง Swagger 2.0 Api documents

1.  เริ่มจากติดตั้ง [Swag](https://github.com/swaggo/swag)

    ```bash
    go install github.com/swaggo/swag/cmd/swag@latest
    ```

2.  รัน [Swag](https://github.com/swaggo/swag) เพื่อสร้างไฟล์ที่จำเป็น (`pkg/docs`folder และ `pkg/docs/doc.go`)

    ```bash
    swag init --parseDependency -g api/api.go -o docs
    ```

3.  สร้างโมดูล swagger `api/swagger/init.go`

    ```go:api/swagger/init.go
    package swagger

    import (
      "fmt"
      "gobasic/config"
      "gobasic/docs"

      "github.com/gofiber/fiber/v2"
      fiberSwagger "github.com/swaggo/fiber-swagger"
    )

    func Init(r fiber.Router, cfg config.Config) {

      //Swagger Doc details
      host := cfg.GatewayHost
      basePath := cfg.GatewayPrefix

      if len(host) == 0 {
        host = fmt.Sprintf("localhost:%d", cfg.Port)
      }

      if len(basePath) == 0 {
        basePath = "/api/v1"
      }

      docs.SwaggerInfo.Title = "Expend API Document"
      docs.SwaggerInfo.Description = "List of APIs for Expend Service."
      docs.SwaggerInfo.Version = "1.0"
      docs.SwaggerInfo.Host = host
      docs.SwaggerInfo.BasePath = basePath
      docs.SwaggerInfo.Schemes = []string{"https", "http"}

      r.Get("/swagger/*", fiberSwagger.WrapHandler)
    }

    ```

4.  แก้ไข `api/api.go` เพื่อใช้ swagger module ในการ generate doc ui ขึ้นมา

    ```go:api/api.go
    func (s *server) setupRouter() {
      // Global Middlewares
      s.app.Use(cors.New())
      s.app.Use(requestid.New())
      s.app.Use(recover.New())
      s.app.Use(logger.New())

      // Register routes here
      api := s.app.Group("/api") // /api
      v1 := api.Group("/v1")     // /api/v1

      swagger.Init(v1, s.config)              // /api/v1/swagger
      users.Init(v1, s.queries, s.tokenMaker) // /api/v1/users

      // สร้าง router สำหรับที่ต้อง authen ก่อนใช้งาน
      authRouter := v1.Group("", middlewares.AuthMiddleware(s.tokenMaker))

      transactions.Init(authRouter, s.queries) // /api/v1/transactions
      dashboards.Init(authRouter, s.queries)   // /api/v1/dashboards
    }
    ```

5.  ทดสอบรัน และเปิด browser ไปที่ [http://localhost:8080/api/v1/swagger/index.html](http://localhost:8080/api/v1/swagger/index.html) ก็จะได้ Swagger 2.0 Api documents

### ใส่รายละเอียด

เพิ่มรายละเอียดให้แต่ละ endpoints รับ request และตอบกลับ response ยังไง จะใช้วิธีการใส่ comments

**Comments ใส่อะไรได้บ้าง**

- @Summary → ใช้บอกว่า api เส้นนี้ทำอะไร
- @Description → ใช้ใส่รายละเอียดแบบยาว
- @Tags → ใช้กำหนด tag ใส่ได้หลาย tag แยกด้วย commas
- @Accept → ใช้กำหนดรูปแบบ request เช่น json
- @Produce → ใช้กำหนดรูปแบบ response เช่น json
- @Param → ใช้กำหนดข้อมูลที่ส่งมา เช่น body ใช้ struct ตัวไหน
  - รูปแบบ `[param name] [param type] [data type] [is mandatory?] [comment] [attribute(optional)]`
  - ตัวอย่าง `term query string false "filter the text based value (ex: term=dosomething)"`
- @Failure → ใช้กำหนด error reponse ทั้ง code และ body ใช้ struct ตัวไหน
  - รูปแบบ `[return code or default] [{param type}] [data type] [comment]`
  - ตัวอย่าง `@Failure 422 {object} dtos.Error422`
- @Success → ใช้กำหนด success reponse ทั้ง code และ body ใช้ struct ตัวไหน
  - รูปแบบ `[return code or default] [{param type}] [data type] [comment]`
  - ตัวอย่าง `@Success 200 {object} dtos.Response`
- @Router → ใช้กำหนด path และ method
  - รูปแบบ `path [httpMethod]`
  - ตัวอย่าง `@Router /transactions [get]`

ดูเพิ่มเติมได้ที่ [https://github.com/swaggo/swag#api-operation](https://github.com/swaggo/swag#api-operation)

## วิธีการทำ CI/CD มา Deploy บน Docker Container

เนื่องจากไม่มี Linux Server จริงๆ สำหรับทำการ Deploy ดังนั้นจะให้วิธีการติดตั้ง Ubuntu Server 22.04 บน Docker แทน เพื่อใช้ในการสาธิตการทำ Continuous deployment (CD)

### เครื่องมือที่ต้องใช้

- [Docker](https://docs.docker.com/engine/install/) & Docker Compose ใช้สำหรับรัน container
- [Ngrok](https://ngrok.com/download) ใช้สำหรับสร้าง tunnel สำหรับการทำ ssh

### ขั้นตอนการเตรียมเครื่อง

1. สร้าง Dockerfile สำหรับ Ubuntu โดยจะติดตั้ง ssh และ docker เพิ่มเข้าไป
2. รัน Ubuntu จาก image ที่สร้างมา
3. เปิดใช้งาน Ngrok
4. ทดสอบการใช้งาน

#### 1. สร้าง Dockerfile สำหรับ Ubuntu โดยจะติดตั้ง ssh และ docker เพิ่มเข้าไป

สามารถข้ามขั้นตอนนี้ได้ โดยใช้ image ที่สร้างไว้แล้วได้จาก [somprasongd/demo-svr-ssh:22.04](https://hub.docker.com/repository/docker/somprasongd/demo-svr-ssh/general)

สร้างไฟล์ชื่อ Dockerfile-Ubuntu

```docker
FROM ubuntu:22.04

LABEL maintainer="Somprasong Damyos <somprasong.damyos@gmail.com>"

# Step 1: ติดตั้งโปรแกรมที่จำเป็นต้องใช้งาน
RUN apt update && \
    apt -qy full-upgrade && \
    apt install \
    openssh-server sudo -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# Step 2: ติดตั้ง Docker
RUN mkdir -m 0755 -p /etc/apt/keyrings && \
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
    echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null && \
    apt update && \
    apt install \
    docker-ce sudo -y \
    docker-ce-cli sudo -y \
    containerd.io sudo -y \
    docker-buildx-plugin sudo -y \
    docker-compose-plugin sudo -y

# Step 3: เพิ่ม user สำหรับการ ssh
RUN useradd -rm -d /home/ubuntu -s /bin/bash -g root -G sudo -u 1000 ost && \
    usermod -aG docker ost

# Step 4: กำหนดรหัสผ่านให้กับ user สำหรับการ ssh
RUN echo 'ost:ost123' | chpasswd

# Step 5: ใส่ public key สำหรับใช้ในการ login ssh
USER ost
RUN mkdir -p /home/ubuntu/.ssh && \
    touch /home/ubuntu/.ssh/authorized_keys && \
    chmod 700 /home/ubuntu/.ssh && \
    chmod 600 /home/ubuntu/.ssh/authorized_keys && \
    echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFrY4W9Ok36VxTehwlqoX4SANPnu5fndWo0Hjx6HTXM+ ost@demo-svr" >> /home/ubuntu/.ssh/authorized_keys

# Step 6: สั่งให้ start ssh server
USER root
RUN service ssh start
EXPOSE 22

# Step 7: กำหนดเมื่อ container ทำงานให้รัน SSH แบบ daemon
CMD ["/usr/sbin/sshd","-D"]
```

### 2. รัน Ubuntu จาก image ที่สร้างมา

- Build Image (ถ้าไม่ได้ทำข้อ 1. ให้ข้ามไปขั้นตอนถัดไป)

  ```bash
  docker image build -t somprasongd/demo-svr-ssh:22.04 -f Dockerfile-Ubuntu .
  ```

- Run Container

  ```bash
  docker run -v /var/run/docker.sock:/var/run/docker.sock --privileged -d -p 22:22 --name=demo-svr somprasongd/demo-svr-ssh:22.04
  ```

  Here's what each option means:

  - **`v /var/run/docker.sock:/var/run/docker.sock`**: This option mounts the Docker daemon socket inside the container, so that the container can communicate with the host's Docker daemon. This is often used when you want to run Docker commands from inside the container.
  - **`-privileged`**: This option gives the container full access to the host system, which is necessary for running certain types of containers.
  - **`d`**: This option runs the container in detached mode, which means it runs in the background and you don't see its output in your terminal.
  - **`p 22:22`**: This option maps port 22 in the container to port 22 on the host, so that you can access the SSH server running inside the container from outside the container.
  - **`-name=demo-svr`**: This option gives the container a name of "demo-svr".
  - **`somprasongd/demo-svr-ssh:22.04`**: This specifies the name and tag of the Docker image to use for the container. In this case, it is "somprasongd/demo-svr-ssh" with a tag of "22.04".

### 3. เปิดใช้งาน Ngrok

- [ติดตั้ง](https://ngrok.com/download)ตาม OS ที่ใช้งาน
- เพิ่ม authtoken

  ```bash
  ngrok config add-authtoken <token>
  ```

- เปิดใช้งาน tunnel

  ```bash
  ngrok tcp 22

  Check which logged users are accessing your tunnels in real time https://ngrok.com/s/app-users

  Session Status  online
  Account         Somprasong Damyos (Plan: Free)
  Version         3.1.1
  Region          Asia Pacific (ap)
  Latency         56ms
  Web Interface   http://127.0.0.1:4040
  Forwarding      tcp://0.tcp.ap.ngrok.io:18224 -> localhost:22
  Connections ttl opn rt1   rt5   p50   p90
              113 0   0.00  0.00  2.83  125.69
  ```

    <aside>
    💡 host คือ 0.tcp.ap.ngrok.io
    port คือ 18224
    
    </aside>

### 4. ทดสอบการใช้งาน

- เข้าใช้งาน ssh

  ```bash
  ssh ost@0.tcp.ap.ngrok.io -p 18224

  ost@0.tcp.ap.ngrok.io's password: ost123
  ```

- ทดสอบใช้งาน docker

  ```bash
  ost@b7e08f3324ec:~$ docker run --rm hello-world
  Unable to find image 'hello-world:latest' locally
  latest: Pulling from library/hello-world
  2db29710123e: Pull complete
  Digest: sha256:6e8b6f026e0b9c419ea0fd02d3905dd0952ad1feea67543f525c73a0a790fefb
  Status: Downloaded newer image for hello-world:latest

  Hello from Docker!
  This message shows that your installation appears to be working correctly.

  To generate this message, Docker took the following steps:
   1. The Docker client contacted the Docker daemon.
   2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
      (amd64)
   3. The Docker daemon created a new container from that image which runs the
      executable that produces the output you are currently reading.
   4. The Docker daemon streamed that output to the Docker client, which sent it
      to your terminal.

  To try something more ambitious, you can run an Ubuntu container with:
   $ docker run -it ubuntu bash

  Share images, automate workflows, and more with a free Docker ID:
   https://hub.docker.com/

  For more examples and ideas, visit:
   https://docs.docker.com/get-started/

  ost@b7e08f3324ec:~$
  ```

  ถ้ามี error ว่า

  ```bash
  docker: Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Post http://%2Fvar%2Frun%2Fdocker.sock/v1.35/containers/create: dial unix /var/run/docker.sock: connect: permission denied.
  See 'docker run --help'.
  ```

  ให้ใช้คำสั่งนี้ `sudo chmod 666 /var/run/docker.sock`

  ```bash
  ost@b7e08f3324ec:~$ sudo chmod 666 /var/run/docker.sock
  ```

  แล้วลองรัน `docker run --rm hello-world` ใหม่อีกครั้ง

### ขั้นตอนการทำ CI/CD โดยใช้ Gitlab-CI

1. สร้างไฟล์ .gitlab-ci.yml
2. ตั้งค่า Variables ใน Gitlab repository
3. Push โค้ดไปยัง Gitlab repository
4. ตรวจสอบการทำงานผ่าน Pipelines

### 1. สร้างไฟล์ .gitlab-ci.yml

```yaml
image: docker:latest
services:
  - docker:dind

stages:
  - test
  - build
  - deploy

variables:
  IMAGE: registry.gitlab.com/ost-pkru/2023/d3-backend/expend-api

before_script:
  - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" "$CI_REGISTRY" --password-stdin

# test:
#   image: golang:latest
#   stage: test
#   before_script:
#     - echo "Running test"
#   script:
#     # - go fmt $(go list ./... | grep -v /vendor/)
#     - go vet $(go list ./... | grep -v /vendor/)
#     - go test -race $(go list ./... | grep -v /vendor/)
#   only:
#     - branches
#   except:
#     - tags

build-dev:
  stage: build
  script:
    - echo "Build docker image from $CI_COMMIT_REF_SLUG, commit $CI_COMMIT_SHORT_SHA"
    # Step 1: Build new image with "dev" tag
    - docker image build -t ${IMAGE}:dev .
    # Step 2: Push new image to docker registry
    - docker image push $IMAGE:dev
  only:
    - dev

build-main:
  stage: build
  script:
    - echo "Build docker image from main"
    # Step 1: Build new image with "latest" tag
    - docker image build -t ${IMAGE}:latest .
    # Step 2: Push new image to docker registry
    - docker image push $IMAGE:latest
  only:
    - main

build-tag:
  stage: build
  script:
    - echo "Build docker image from $CI_COMMIT_TAG"
    # Step 1: Pull latest image from registry
    - docker image pull $IMAGE:latest
    # Step 2: Tag new version from "latest" image
    - docker image tag ${IMAGE}:latest ${IMAGE}:$CI_COMMIT_TAG
    # Step 3: Push new image to docker registry
    - docker image push $IMAGE:$CI_COMMIT_TAG
  only:
    - tags

deploy-dev:
  image: alpine:3.8
  stage: deploy
  dependencies:
    - build-dev
  variables:
    BASE_DIR: ~/docker/expend-app/api
    SSH_USER_SERVER: $DEV_SSH_USER@$DEV_SSH_HOST
    SSH_SERVER: $DEV_SSH_HOST
    SSH_SERVER_PORT: $DEV_SSH_PORT
    SSH_PRIVATE_KEY: $DEV_SSH_PRIVATE_KEY
    IMAGE_VERSION: dev
  before_script:
    # Prepare environtment for ssh to a server
    - apk add --update openssh-client
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | ssh-add -
    - mkdir -p ~/.ssh
    - ssh-keyscan -p $SSH_SERVER_PORT $SSH_SERVER >> ~/.ssh/known_hosts
  script:
    # Step 1: Create app directory "~/expend-app/api"
    - ssh $SSH_USER_SERVER -p $SSH_SERVER_PORT mkdir -p $BASE_DIR
    # Step 2: Copy docker-compose.yml to ~/expend-app/api/docker-compose.yml
    - scp -P $SSH_SERVER_PORT -o stricthostkeychecking=no -r ./docker-compose.yml $SSH_USER_SERVER:$BASE_DIR/docker-compose.yml
    # Step 3: Login to gitlab registry
    - ssh $SSH_USER_SERVER -p $SSH_SERVER_PORT docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    # Step 4: Pull new image
    - ssh $SSH_USER_SERVER -p $SSH_SERVER_PORT "cd $BASE_DIR&&IMAGE_VERSION=$IMAGE_VERSION docker compose pull api"
    # Step 5: Deploy
    - ssh $SSH_USER_SERVER -p $SSH_SERVER_PORT "cd $BASE_DIR&&IMAGE_VERSION=$IMAGE_VERSION docker compose up -d"
    # Step 6: Clean up (remove unused images)
    - ssh $SSH_USER_SERVER -p $SSH_SERVER_PORT "docker image prune -f"
  only:
    - dev
```

### 2. ตั้งค่า Variables ใน Gitlab repository

- ไปที่เมนู `Setting → CI/CD → Variables`
- กดปุ่ม Expand
- กดปุ่ม Add Variable เพื่อเพิ่มรายการใหม่ ดังนี้

  - `DEV_SSH_PRIVATE_KEY`

    - **Value**: ใช้ค่าตามนี้เลย

      ```bash
      ----BEGIN OPENSSH PRIVATE KEY-----
      b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
      QyNTUxOQAAACBa2OFvTpN+lcU3ocJaqF+EgDT57uX53VqNB48eh01zPgAAAJBsG0EDbBtB
      AwAAAAtzc2gtZWQyNTUxOQAAACBa2OFvTpN+lcU3ocJaqF+EgDT57uX53VqNB48eh01zPg
      AAAEDogHc4HsRpXPO07iceEJhFA9njY0w4jCHXOyW4bv0s01rY4W9Ok36VxTehwlqoX4SA
      NPnu5fndWo0Hjx6HTXM+AAAADG9zdEBkZW1vLXN2cgE=
      -----END OPENSSH PRIVATE KEY---—
      ```

    - **Protect variable**: ไม่เลือกถูก
    - **Mask variable**: ไม่เลือกถูก
    - **Expand variable reference**: เลือกถูก

  - `DEV_SSH_HOSTKEYS`

    - **Value**: ได้มาจากการรัน `cat ~/.ssh/known_hosts` โดยเลือกเอาที่ขึ้นต้น `[0.tcp.ap.ngrok.io]` มาทั้งหมด ตัวอย่างเช่น

      ```bash
      [0.tcp.ap.ngrok.io]:18224 ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAID1QROMe3ZPTDKiZcLkup6q9/5uEu4J4usibqSj9BXPf
      [0.tcp.ap.ngrok.io]:18224 ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCesj0ANrsy9aFh1sPR8V7QnB+2xiUbUAJW0wEe+QSKcHixr+UN2qF+o2G3ybMOUoEEEn5O64e/EHoocHKaQ0HZBGN+3Z9wN6Vfi3yDx5i1euoTX0xUw8s5umHO7X6R41AYNOxrfWmQMdchnY+feNPEV4HA72bHJpIA67OlEHQUtJXM1zn26zmHsIvCKKvzxir92Hq2R1oTg0PtlVMBbdDA3FuXhD5ZGo7Ucdk3Nbt6hZXcJh2joZZntUMI3ZUew2u9B7M+ir7hAx9fdImjeBkTfbd/jmtemwX97VcDfUozNHQM+poGQT6sZ9M8nJM1l+oUWeJxtdzpJbukUOiLqGEbnWykdiZCzqOeJ2DyyRWDI0n9g0SmBz4FnKhY+yZZM9hHvD/83IM7cR1mEoPT+S4qhPkpsR4oCdHdlxP5R6A5lI5OhN0vZuxxNEQSjCJYnTGJlHLJj3xhte7NSErAjkvIjgIdHH7RkooSW5BWkDXaDj0LqWqQfXxe11AKnkVQ93M=
      [0.tcp.ap.ngrok.io]:18224 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJkxMazTL+0TRqTWSiF5A5ZsVVqyASDxJFHyRaj8gbfC0TeeI3h0QA7SLuIHgaqljwcMXjw47ncZ2/gfR7qbEOI=
      ```

    - **Protect variable**: ไม่เลือกถูก
    - **Mask variable**: ไม่เลือกถูก
    - **Expand variable reference**: เลือกถูก

  - `DEV_SSH_HOST`
    - **Value**: `0.tcp.ap.ngrok.io`
    - **Protect variable**: ไม่เลือกถูก
    - **Mask variable**: ไม่เลือกถูก
    - **Expand variable reference**: เลือกถูก
  - `DEV_SSH_PORT`
    - **Value**: `18224` _(ซึ่งได้มาจากการรัน ngrok)_
    - **Protect variable**: ไม่เลือกถูก
    - **Mask variable**: ไม่เลือกถูก
    - **Expand variable reference**: เลือกถูก
  - `DEV_SSH_USER`
    - **Value**: `ost`
    - **Protect variable**: ไม่เลือกถูก
    - **Mask variable**: ไม่เลือกถูก
    - **Expand variable reference**: เลือกถูก

### 3. Push โค้ดไปยัง Gitlab repository

ให้ทำการ commit และ push โค้ดไปยัง Gitlab repository

### 4. ตรวจสอบการทำงานผ่าน Pipelines

ตรวจสอบการทำงานของ Pipelines ที่เมนู `CD/CD → Pipelines`
