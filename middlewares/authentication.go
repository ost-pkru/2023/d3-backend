package middlewares

import (
	"fmt"
	"gobasic/util/env"
	"gobasic/util/token"
	"strings"

	"github.com/gofiber/fiber/v2"
)

const (
	authorizationHeaderKey  = "authorization"
	authorizationTypeBearer = "bearer"
	AuthorizationPayloadKey = "authorization_payload"
)

// AuthMiddleware creates a fiber middleware for authorization
func AuthMiddleware(tokenMaker *token.JWTMaker) fiber.Handler {
	return func(c *fiber.Ctx) error {
		authorizationHeader := c.Get(authorizationHeaderKey)

		if len(authorizationHeader) == 0 {
			// if no authorizationHeader and use demo user fix user id
			if useDemo := env.GetEnvBool("ENABLE_DEMO_USER", false); useDemo {
				// use demo user id
				payload := token.Payload{
					Sub: "1c0dc346-bd24-4aa1-bf41-acd62dd325d6",
				}
				c.Locals(AuthorizationPayloadKey, &payload)
				return c.Next()
			}
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
				"error": "authorization header is not provided",
			})
		}

		fields := strings.Fields(authorizationHeader)
		if len(fields) < 2 {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
				"error": "invalid authorization header format",
			})
		}

		authorizationType := strings.ToLower(fields[0])
		if authorizationType != authorizationTypeBearer {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
				"error": fmt.Sprintf("unsupported authorization type %s", authorizationType),
			})
		}

		accessToken := fields[1]
		payload, err := tokenMaker.VerifyToken(accessToken)
		if err != nil {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
				"error": err.Error(),
			})
		}

		c.Locals(AuthorizationPayloadKey, payload)
		return c.Next()
	}
}
