package greet

import "fmt"

func Hello(name string) {
	fmt.Println("Hello,", name)
}
